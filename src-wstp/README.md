April 26, 2015

--------------------------------------------------------
*WSTP* interface to C-Libray *cddlib* README FILE
--------------------------------------------------------

1. This directory contains additional files necessary to create 
*cddwstp* and *cddwstpgmp*. It does not follow the auto-config 
structure and the user must edit `Makefile` so that it conforms 
to the local *Mathematica* and *cddlib* installation.


2. *Mathematica WSTP* executables and library files must be installed 
properly. In current version of *Mathematica* (9 and 10), the 
executables *wscc* and *wsprep*, the libraries `libWSTPi3.a` and 
`libWSTPi4.a`, the header file `wstp.h` are all installed in dictionary  
*/Applications/Mathematica.app/SystemFiles/Links/WSTP/DeveloperKit/MacOSX-x86-64/CompilerAdditions*.


3. Before *cddwstp(gmp)* compilation, one must compile the *cddlib* 
libraries (`libcdd.a` and `libcddgmp.a`) which are supposed to
be in the *lib-src* and *lib-src-gmp* subdirectories of *cddlib*.
The distribution of *cddlib* comes with the necessary *GMP*. If one
compiles the *cddlib* manually from the distribution, the *GMP* 
will be compiled first automatically. But it’s recommended to use 
*Homebrew* on *Macintosh* to compile *GMP* and *cddlib*, then all 
necessary libraries including `libcdd.a` and `libcddgmp.a` can be 
found in directory    
*/usr/local/lib*     
via symbolic links


4. Examples for how to use *cddwstp* and *cddwstpgmp* can be found in
subdirectory *examples-wstp* of *cddlib*.
